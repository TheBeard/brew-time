require('dotenv').config();
const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccount.json');

const credential = admin.credential.cert(serviceAccount);

const app = admin.initializeApp({
  credential,
  databaseURL: 'https://brew-time-edc30.firebaseio.com',
});

const firestore = app.firestore();
firestore.settings({ timestampsInSnapshots: true });

const potsCollection = firestore.collection('pots');


Promise.all([
  potsCollection
    .add({
      estimatedBrewTime: 480000,
      location: '200 Bathurst, Downstairs',
      name: 'Dark Roast',
    }),

  potsCollection
    .add({
      estimatedBrewTime: 480000,
      location: '200 Bathurst, Downstairs',
      name: 'Medium Roast',
    }),
])
  .catch((err) => {
    console.log('Could not seed database', err);
  })
  .then(() => process.exit(0));
