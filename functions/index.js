// const express = require('express');
const functions = require('firebase-functions');
const axios = require('axios');
const admin = require('firebase-admin');

admin.initializeApp();
admin.firestore().settings({ timestampsInSnapshots: true });

// const app = express();
//
// const topicForPotId = (potId) => `brew-${potId}`;
//
// app.post('/token', (request, response) => {
//   if (!request.body.token) return response.json({ message: 'No token' }).status(400).end();
//   if (!request.body.potId) return response.json({ message: 'No potId' }).status(400).end();
//
//   admin.messaging().subscribeToTopic([request.body.token], topicForPotId(request.body.potId))
//     .then(function(response) {
//       response.status(200).end();
//     })
//     .catch(function(error) {
//       console.log('Error subscribing to topic:', error);
//       response.json({ message: error.code }).status(500).end();
//     });
// });
//
// app.delete('/token', (request, response) => {
//   if (!request.body.token) return response.json({ message: 'No token' }).status(400).end();
//   if (!request.body.potId) return response.json({ message: 'No potId' }).status(400).end();
//
//   admin.messaging().unsubscribeToTopic([request.body.token], topicForPotId(request.body.potId))
//     .then(function(response) {
//       response.status(200).end();
//     })
//     .catch(function(error) {
//       console.log('Error subscribing to topic:', error);
//       response.json({ message: error.code }).status(500).end();
//     });
// });
//
// exports.handleUserToken = functions.https.onRequest(app);
//

const reportBrewToSlack = (snap, context) => {
  return snap.ref.parent.parent
    .get()
    .then((potSnap) => {
      const pot = potSnap.data();
      return axios.post(functions.config().slack.messageurl, {
        text: `Your ${pot.name} @ ${pot.location} will be done in about ${Math.ceil(pot.estimatedBrewTime / 60000)} minutes`,
      });
    });
};

const logEvent = (message, event) => {
  admin.firestore().collection('/logs').add({
    createdAt: admin.firestore.Timestamp.now(),
    message,
    event,
  });
};

const getUser = (userId) => {
  return admin
    .auth()
    .getUser(userId)
    .then((user) => {
      return user.displayName || user.email || `Anonymous_${user.uid.slice(0, 5)}`;
    })
    .catch(() => {
      return 'anonymous (no account)'
    });
};

exports.onBrewCreated = functions.firestore.document('/pots/{potId}/brews/{brewId}').onCreate((snap, context) => {
  const brew = snap.data();
  return snap.ref.parent.parent.get()
    .then(snap => snap.data())
    .then(pot => Promise.all([
        reportBrewToSlack(snap, context),
        getUser(snap.data().brewedBy).then(name => logEvent(`New brew for ${pot.name} @ ${pot.location} started by ${name}`, 'brew.created')),
    ]));
});

exports.onUserCreated = functions.auth.user().onCreate((event) => {
  const userName = event.data.displayName || event.data.email || 'anonymous';
  return Promise.all([
    logEvent(`New user ${userName} created`, 'auth.created'),
  ]);
});
