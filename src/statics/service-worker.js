const version = '__SERVICE_WORKER_VERSION__';

const onInstall = () => fetch('./main.js')
  .then(response => caches
    .open(version)
    .then(cache => cache.put('main.js', response))
  )
  .then(() => self.skipWaiting());

self.addEventListener('install', (event) => {
  event.waitUntil(onInstall());
});

self.addEventListener('activate', (event) => {
  const deleteCache = (cachedName) => {
    if (cachedName !== version) {
      return caches.delete(cachedName);
    }
    return Promise.resolve();
  }

  const invalidateCaches = () => {
    return caches.keys()
      .then((cachedNames) => Promise.all(cachedNames.map(deleteCache)))
      .then(() => self.clients.claim());
  }

  event.waitUntil(invalidateCaches());
});

self.addEventListener('fetch', (event) => {
  if (event.request.url.includes('/main.js')) {
    event.respondWith(caches
      .open(version)
      .then(cache => cache
        .match('main.js')
        .then((response) => {
          if (!response) {
            console.error('Missing cache!');
          }
          return response;
        })
      )
    );
  }
  if (event.request.url.includes('/version')) {
    event.respondWith(new Response(version, {
      headers: {
        'content-type': 'text/plain',
      }
    }));
  }
})

importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

const app = firebase.initializeApp({
  messagingSenderId: '752376989190',
});

const sendNotification = () => {
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
  };

  return self.registration
    .showNotification(notificationTitle, notificationOptions);
};

app.messaging().setBackgroundMessageHandler((payload) => {
  console.log('service-worker', 'setBackgroundMessageHandler', payload);
  return sendNotification();
});
