const firebase = require('firebase/app');
require('firebase/auth');
require('firebase/firestore');
try {
  require('firebase/messaging');
} catch(err) {
  console.log(err);
}

const firebaseApp = firebase.initializeApp({
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  projectId: process.env.FIREBASE_PROJECT_ID,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
});

try {
  firebaseApp.messaging().usePublicVapidKey(process.env.PUBLIC_VAPID_KEY);
} catch(err) {
  console.log(err);
}

const query = {
  user: userId => firebaseApp.firestore().collection('users').doc(userId),
  pot: potId => firebaseApp.firestore().collection('pots').doc(potId),
  brew: (potId, brewId) => (
    firebaseApp
      .firestore()
      .collection('pots')
      .doc(potId)
      .collection('brews')
      .doc(brewId)
  ),
  latestBrew: potId => (
    firebaseApp
      .firestore()
      .collection('pots')
      .doc(potId)
      .collection('brews')
      .limit(1)
      .orderBy('brewedAt', 'desc')
  ),
  addBrew: (potId, user) => (
    firebaseApp
      .firestore()
      .collection('pots')
      .doc(potId)
      .collection('brews')
      .add({
        brewedBy: user ? user.uid : `anonymous@${Date.now()}`,
        brewedAt: firebase.firestore.FieldValue.serverTimestamp(),
      })
  ),
  subscriptionsByUserId: userId => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
      .where('userId', '==', userId)
  ),
  subscribeToPotsFromSubscriptions: ({ potCallback, brewCallback, subscriptions }) => (
    subscriptions.reduce((cancelCallbacks, subscription) => (
      {
        ...cancelCallbacks,
        [subscription.potId]: {
          pot: firebaseApp
            .firestore()
            .collection('pots')
            .doc(subscription.potId)
            .onSnapshot(potCallback),
          brew: firebaseApp
            .firestore()
            .collection('pots')
            .doc(subscription.potId)
            .collection('brews')
            .limit(1)
            .orderBy('brewedAt', 'desc')
            .onSnapshot((docSnap) => brewCallback({ docSnap, potId: subscription.potId })),
        },
      }
    ), {})
  ),
  subscriptionCollection: () => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
  ),
  subscription: subId => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
      .doc(subId)
  ),
  subscribeToPot: ({ userId, potId, enableNotifications = false }) => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
      .add({
        userId,
        potId,
        enableNotifications,
      })
  ),
  unsubscribeFromPot: ({ subscriptionId }) => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
      .doc(subscriptionId)
      .delete()
  ),
  updateSubscriptionToPot: ({ subscriptionId, enableNotifications }) => (
    firebaseApp
      .firestore()
      .collection('subscriptions')
      .doc(subscriptionId)
      .update({
        enableNotifications,
      })
  ),
};

const timeNow = () => firebase.firestore.Timestamp.now();
const timeFrom = timestamp => firebase.firestore.Timestamp.fromMillis(timestamp);

module.exports = {
  module: firebase,
  app: firebaseApp,
  query,
  timeNow,
  timeFrom,
};
