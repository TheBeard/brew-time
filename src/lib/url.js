// TODO: Add base url to env file, and use that here instead of document because
// it is a side-effect.
const forPot = potId => [
  document.location.origin,
  potId,
].filter(Boolean).join('/');

module.exports = {
  forPot,
};
