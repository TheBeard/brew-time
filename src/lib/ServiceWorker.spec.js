const test = require('ava');
const sinon = require('sinon');
const { getRegistration, register } = require('./ServiceWorker');

test.cb('.getRegistration returns the ready promise when a controller is already present', (t) => {
  t.plan(1);

  const navigator = {
    serviceWorker: {
      controller: true,
      ready: Promise.resolve(),
      register: () => {
        throw new Error('Oh no, register shouldn\'t run');
      },
    },
  };

  getRegistration(navigator)
    .then(() => {
      t.pass('Used ready promise');
    })
    .catch((e) => {
      t.fail(e.message);
    })
    .then(() => {
      t.end();
    });
});

test.cb('.getRegistration returns the registration promise when a controller is already present', (t) => {
  t.plan(3);

  const navigator = {
    serviceWorker: {
      controller: null,
      ready: null,
      register: (serviceWorkerPath, options) => {
        t.is(serviceWorkerPath, 'service-worker.js');
        t.is(options.scope, '/');
        return Promise.resolve(new Object());
      },
    },
  };

  getRegistration(navigator)
    .then(() => {
      t.pass();
    })
    .catch((e) => {
      t.fail(e.message);
    })
    .then(() => {
      t.end();
    });
});

test.cb('.register calls getRegistration and sets data for firebase push notifications', (t) => {
  t.plan(2);

  const navigator = {
    serviceWorker: {
      controller: null,
      ready: null,
      register: (serviceWorkerPath, options) => {
        return Promise.resolve(new Object());
      },
    },
  };

  const messaging = {
    useServiceWorker: sinon.fake(),
    getToken: sinon.fake(() => Promise.resolve()),
  };

  register(navigator, messaging)
    .then(() => {
      t.is(messaging.useServiceWorker.callCount, 1);
      t.is(messaging.getToken.callCount, 1);
    })
    .catch((e) => {
      t.fail(e.message);
    })
    .then(() => {
      t.end();
    });
});
