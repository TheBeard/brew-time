const getRegistration = (navigator) => {
  if (!navigator.serviceWorker) return Promise.reject('Unsupported browser');
  if (navigator.serviceWorker.controller) {
    return navigator.serviceWorker.ready;
  }

  return navigator.serviceWorker
    .register('service-worker.js', {
      scope: '/',
    });
};

// TODO: Once there are API calls, add some mock api tests
const register = (navigator, messaging) => {
  return getRegistration(navigator)
    .then((registration) => {
      messaging.useServiceWorker(registration);
      messaging.getToken()
        .then((token) => {
          // TODO:
          //  - Add firebase function that listens to POST /register-push-token
          //  - Add webpack-serve route to handle same route
          //  - This needs to do a proper post to subscribe a token to a topic
          //
        })
        .catch((err) => {
          console.log('Could not get token', err);
        });
    });
};

module.exports = {
  getRegistration,
  register,
}
