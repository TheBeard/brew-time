class User {
  constructor(firebaseUser) {
    this.firebaseUser = firebaseUser;
  }

  isSignedIn() {
    return undefined;
  }

  isAnonymous() {
    return undefined;
  }

  displayName() {
    return undefined;
  }
}

class SignedOutUser extends User {
  isSignedIn() {
    return false;
  }
}

class SignedInUser extends User {
  constructor(firebaseUser) {
    super(firebaseUser);
    if (!firebaseUser) {
      throw new Error(
        'SignedInUser requires a valid firebase user',
      );
    }
  }

  isSignedIn() {
    return true;
  }

  isAnonymous() {
    return this.firebaseUser.isAnonymous;
  }

  displayName() {
    if (this.isAnonymous()) {
      return `Anonymous_${this.firebaseUser.uid.slice(0, 5)}`;
    }
    return this.firebaseUser.displayName || this.firebaseUser.email;
  }
}

User.fromUser = (firebaseUser) => {
  if (firebaseUser) {
    return new SignedInUser(firebaseUser);
  }
  return new SignedOutUser();
}

module.exports = {
  User,
  SignedOutUser,
  SignedInUser,
};
