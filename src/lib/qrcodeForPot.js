const qrcode = require('qrcode');
const url = require('./url');

const qrcodeForPot = (potId) => {
  const canvas = document.createElement('canvas');
  canvas.width = 256;
  canvas.height = 256;

  return new Promise((resolve, reject) => {
    qrcode.toCanvas(canvas, url.forPot(potId), (err) => {
      if (err) return reject(err);
      return resolve(canvas.toDataURL('image/png'));
    });
  });
};

module.exports = {
  qrcodeForPot,
};
