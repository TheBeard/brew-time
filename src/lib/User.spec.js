const test = require('ava');
const { User, SignedOutUser, SignedInUser } = require('./User');

test('User stores the firebase user', (t) => {
  const firebaseUser = new Object();
  const user = new User(firebaseUser);
  t.is(user.firebaseUser, firebaseUser);
});

test('User has undefined returns for all member methods', (t) => {
  t.plan(3);

  const user = new User();
  t.is(user.isSignedIn(), undefined);
  t.is(user.isAnonymous(), undefined);
  t.is(user.displayName(), undefined);
});

test('SignedOutUser inherits from User', (t) => {
  const user = new SignedOutUser();
  t.is(user instanceof User, true);
});

test('SignedOutUser returns false for isSignedIn', (t) => {
  const user = new SignedOutUser();
  t.is(user.isSignedIn(), false);
});

test('SignedInUser throws if no firebaseUser is passed', (t) => {
  const shouldThrow = () => {
    const user = new SignedInUser();
  };
  t.throws(shouldThrow, /requires a valid firebase user/i);
});

test('SignedInUser inherits from User', (t) => {
  const user = new SignedInUser(new Object());
  t.is(user instanceof User, true);
});

test('SignedInUser returns true for isSignedIn method', (t) => {
  const user = new SignedInUser(new Object());
  t.is(user.isSignedIn(), true);
});

test('SignedInUser returns the value of firebaseUser.isAnonymous for isAnonymous method', (t) => {
  t.plan(2);

  const anonymousUser = new SignedInUser({ isAnonymous: true });
  const knownUser = new SignedInUser({ isAnonymous: false });

  t.is(anonymousUser.isAnonymous(), true);
  t.is(knownUser.isAnonymous(), false);
});

test('SignedInUser displayName returns the correct name based on whether the user is anonymous, has a displayname, or at the very least has an email', (t) => {
  t.plan(4);

  const userCases = [
    { user: { isAnonymous: true, uid: 'abc123def456' }, expected: 'Anonymous_abc12' },
    { user: { isAnonymous: false, uid: 'abc123def456', displayName: null, email: 'test@test.com' }, expected: 'test@test.com' },
    { user: { isAnonymous: false, uid: 'abc123def456', displayName: 'TestName', email: null }, expected: 'TestName' },
    { user: { isAnonymous: false, uid: 'abc123def456', displayName: 'TestName', email: 'test@test.com' }, expected: 'TestName' },
  ];

  userCases.forEach(({ user, expected }) => {
    const u = new SignedInUser(user);
    t.is(u.displayName(), expected);
  });
});

test('User.fromUser returns either a SignedInUser or SignedOutUser, depending on if firebaseUser is a truthy value', (t) => {
  t.plan(2);

  const signedInUser = User.fromUser(new Object());
  t.is(signedInUser instanceof SignedInUser, true);

  const signedOutUser = User.fromUser(null);
  t.is(signedOutUser instanceof SignedOutUser, true);
});
