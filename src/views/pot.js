const { h } = require('hyperapp');
const Brew = require('../components/Brew');
const { query, timeFrom } = require('../lib/firebase');
const { qrcodeForPot } = require('../lib/qrcodeForPot');

const initialState = {
  subscription: null,
  pot: null,
  brew: null,

  qrcode: null,

  unsubscribeFromSubscription: null,
  unsubscribeFromPot: null,
  unsubscribeFromBrew: null,
};

const initialActions = {
  create: ({ potId, user }) => (state, actions) => {
    qrcodeForPot(potId)
      .then(actions.setQrcode)
      .catch((err) => {
        console.log('cannot set qrcode', err);
      });

    return {
      unsubscribeFromPot: query.pot(potId).onSnapshot(actions.setPot),
      unsubscribeFromBrew: query.latestBrew(potId).onSnapshot(actions.setBrew),
      unsubscribeFromSubscription: user
        ? query
          .subscriptionsByUserId(user.uid)
          .where('potId', '==', potId)
          .limit(1)
          .onSnapshot(actions.setSubscription)
        : null,
    }
  },

  destroy: () => (state) => {
    state.unsubscribeFromPot();
    state.unsubscribeFromBrew();
    if (state.unsubscribeFromSubscription) state.unsubscribeFromSubscription();
    return {
      unsubscribeFromPot: null,
      unsubscribeFromBrew: null,
    };
  },

  setQrcode: qrcode => ({ qrcode }),

  setPot: docSnap => (state) => {
    const pot = {
      ...docSnap.data(),
      id: docSnap.id,
    };

    return {
      pot,
    };
  },

  setBrew: docSnap => (state) => {
    const doc = docSnap.docs[0];
    if (!doc) return undefined;
    const brew = {
      ...doc.data(),
      id: doc.id,
    };

    return {
      brew,
    };
  },

  setSubscription: docSnap => (state) => {
    const doc = docSnap.docs[0];
    if (!doc) return { subscription: null };
    const subscription = {
      ...doc.data(),
      id: doc.id,
    };

    return {
      subscription,
    };
  },

  subscribeToPot: ({ potId, user }) => (state) => {
    return query.subscribeToPot({ userId: user.uid, potId });
  },

  unsubscribeToPot: () => (state) => {
    const { subscription } = state;
    return query.unsubscribeFromPot({ subscriptionId: subscription.id });
  },

  addBrew: ({ potId, user }) => {
    return query.addBrew(potId, user);
  },
};

const view = ({ state, actions, route }) => {
  const originalBrew = state.brew || {
    brewedBy: null,
    brewedAt: null,
    brewDoneAt: null,
    id: null,
  };

  const brew = {
    pot: state.pot,
    potId: route.match.params.id,
    subscription: state.subscription,
    ...originalBrew,
  };

  return h(
    'div',
    {
      key: 'show',
      oncreate: () => {
        actions.create({ potId: brew.potId, user: state.user });
      },
      ondestroy: () => {
        actions.destroy();
      },
    },
    [
      h(
        'div',
        {
          style: {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            justifyContent: 'center',
          }
        },
        [
          h(
            Brew.component,
            {
              brew,
              qrcode: state.qrcode,
              user: state.user,
              now: state.now,
              notifications: state.notifications,
              addBrew: actions.addBrew,
              subscribeToNotifications: actions.subscribeToNotifications,
              subscribeToPot: actions.subscribeToPot,
              unsubscribeToPot: actions.unsubscribeToPot,
            },
          )
        ]
      ),
    ],
  );
};

module.exports = {
  state: initialState,
  actions: initialActions,
  view,
};
