const { h } = require('hyperapp');
const jsqr = require('jsqr');
const Brew = require('../components/Brew');
const Button = require('../components/Button');
const { query, timeFrom } = require('../lib/firebase');
require('./home.styl');

const initialState = {
  subscriptions: [],
  pots: [],
  brews: [],

  unsubscribeFromUserSubscriptions: null,
  unsubscribeFromUserPots: {},

  mediaSource: null,
  imageCapture: null,
  captureTimeout: null,
};

const unsubscribeFrom = (unsubscribeFromUserPots, potId) => {
  const { pot, brew } = unsubscribeFromUserPots[potId];
  if (typeof pot === 'function') pot();
  if (typeof brew === 'function') brew();

  return {
    ...unsubscribeFromUserPots,
    [potId]: undefined,
  };
}

const validUrlStart = [
  "http://localhost",
  "https://localhost",
  "https://brew-time-dev.firebaseapp.com",
  "https://beta.brewtime.coffee",
  "https://brewtime.coffee",
]

const qrCodeValid = (decoded) => {
  if (!decoded) return false;
  return validUrlStart.some((urlStart) => decoded.data.startsWith(urlStart));
}

const extractQrCodePotId = (decoded) => {
  const prefix = validUrlStart.find(vUS => decoded.data.startsWith(vUS));
  const pieces = decoded.data.replace(prefix, '').replace(/\?.+/, '').split('/').filter(s => s.length > 1)
  return pieces[pieces.length - 1];
}

const initialActions = {
  create: user => (state, actions) => {
    if (user) actions.listenToUserSubscriptions(user);
  },

  destroy: () => (state) => {
    if (state.unsubscribeFromUserSubscriptions) state.unsubscribeFromUserSubscriptions();
    Object.keys(state.unsubscribeFromUserPots).forEach((potId) => {
      unsubscribeFrom(state.unsubscribeFromUserPots, potId);
    });

    return {
      unsubscribeFromUserSubscriptions: null,
      unsubscribeFromUserPots: {},
    };
  },

  listenToUserSubscriptions: user => (state, actions) => {
    const unsubscribeFromUserSubscriptions = query
      .subscriptionsByUserId(user.uid)
      .onSnapshot(actions.addUserSubscriptions);

    return {
      unsubscribeFromUserSubscriptions,
    };
  },

  addUserSubscriptions: collectionRef => (state, actions) => {
    Object.keys(state.unsubscribeFromUserPots).forEach((potId) => {
      unsubscribeFrom(state.unsubscribeFromUserPots, potId);
    });

    const subscriptions = collectionRef.docs.map((subscriptionDoc) => ({
      ...subscriptionDoc.data(),
      id: subscriptionDoc.id,
    }));

    const unsubscribeFromUserPots = query
      .subscribeToPotsFromSubscriptions({ potCallback: actions.addUserPot, brewCallback: actions.addUserBrew, subscriptions })

    return {
      subscriptions,
      unsubscribeFromUserPots,
    };
  },

  addUserPot: docSnap => (state) => {
    const pot = {
      ...docSnap.data(),
      id: docSnap.id,
    };

    return {
      pots: state.pots.filter(p => p.id !== pot.id).concat(pot),
    };
  },

  addUserBrew: ({ potId, docSnap }) => (state) => {
    const doc = docSnap.docs[0];
    if (!doc) return undefined;

    const brew = {
      ...doc.data(),
      potId,
      id: docSnap.id,
    };


    return {
      brews: state.brews
        .filter(b => (b.id !== brew.id) || (b.potId !== potId))
        .concat(brew),
    };
  },

  removePotAndBrews: potId => state => ({
    pots: state.pots.filter(p => p.id !== potId),
    brews: state.brews.filter(b => b.potId !== potId),
  }),

  subscribeToPot: ({ potId, user, subscribe }) => (state, actions) => {
    return query
      .subscribeToPot({
        potId,
        userId: user.uid,
      });
  },

  unsubscribeToPot: ({ subscription }) => (state, actions) => {
    return query
        .unsubscribeFromPot({ subscriptionId: subcription.id })
        .then(() => {
          actions.removePotAndBrews(subscription.potId);
        });
  },

  addBrew: ({ potId, user }) => {
    return query.addBrew(potId, user);
  },

  checkQrCode: () => (state, actions) => {
    return navigator.mediaDevices
      .getUserMedia({ video: {
        facingMode: 'environment',
      }})
      .then(mediaSource => {
        const captureTimeout = setTimeout(() => {
          actions.doneCapture(false);
        }, 10000);
        actions.beginCapture({ captureTimeout, mediaSource });
      })
      .catch((err) => {
        console.log(err);
      });
  },

  beginCapture: ({ captureTimeout, mediaSource }) => (state, actions) => {
    if (state.captureTimeout) clearTimeout(state.captureTimeout);
    setTimeout(actions.processStreamFrame, 1);
    const tracks = mediaSource.getTracks();
    return {
      captureTimeout,
      mediaSource,
      imageCapture: new ImageCapture(tracks[0]),
    };
  },

  processStreamFrame: () => (state, actions) => {
    if (!state.imageCapture) return;

    return state.imageCapture
      .grabFrame()
      .then(bitmap => {
        const canvas = document.createElement('canvas');
        canvas.width = bitmap.width;
        canvas.height = bitmap.height;
        const context = canvas.getContext('2d');
        context.drawImage(bitmap, 0, 0);
        return context.getImageData(0, 0, bitmap.width, bitmap.height);
      })
      .then((imageData) => {
        const decoded = jsqr(imageData.data, imageData.width, imageData.height);
        const isDecodeValid = qrCodeValid(decoded);
        if (isDecodeValid) {
          const potId = extractQrCodePotId(decoded);
          return actions.doneCapture(potId);
        } else {
          setTimeout(actions.processStreamFrame, 1);
        }
      });
  },

  doneCapture: (potId) => (state) => {
    if (state.mediaSource) state.mediaSource.getTracks().forEach(track => track.stop());
    if (state.captureTimeout) clearTimeout(state.captureTimeout);
    if (potId) {
      if (navigator.vibrate) {
        navigator.vibrate(1000);
      }
      setTimeout(() => {
        history.pushState(null, "", `/${potId}`);
      }, 1000);
    } else if (potId !== false) {
      alert('Couldn\'t find a QR Code in 10 seconds. Please try again.');
    }
    return {
      mediaSource: null,
      imageCapture: null,
      captureTimeout: null,
    }
  },
};

const qrCodeOverlay = ({ mediaSource, imageCapture, doneCapture }) => mediaSource && h(
  'div',
  {
    class: 'home__overlay',
    onclick: (e) => {
      e.preventDefault();
      if (e.target === e.currentTarget) {
        doneCapture(false);
      }
    },
  },
  [
    h('h2', null, 'Searching for QR Code'),
    h('small', null, 'Checking for 10 seconds'),
    h(
      'video',
      {
        oncreate: (element) => {
          const tracks = mediaSource.getTracks();
          tracks.forEach((track, index) => {
            track.selected = index === 0;
          });
          element.srcObject = mediaSource; // = URL.createObjectURL(mediaSource);
          element.play();
        },
      }
    ),
  ]
);

const scanQrCode = (checkQrCode) => h(
  Button.component,
  {
    type: 'button',
    style: {
      lineHeight: 1,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '0 50px',
      height: '300px',
      width: '200px',
      color: 'white',
    },
    onclick: checkQrCode,
  },
  '+ Scan Brew'
);

const instructions = ({ checkQrCode, mediaSource, doneCapture }) => h(
  'div',
  {
    class: 'home__instructions-header',
  },
  [
    h('h1', null, 'Welcome to Brew Time'),
    h('h2', null, 'Scan it, brew it'),
    h('p', null, [
      'Brew time lets you know when the last brew was made, so you avoid drinking yesterday\'s room tempurature coffee.',
    ]),
    h('p', null, [
      'Want to get more out of the app? Sign in to get notifications on when a new brew is on it\'s way!',
    ]),
  ],
);

const content = ({
  now,
  brews,
  user,
  mediaSource,
  notifications,
  addBrew,
  subscribeToNotifications,
  subscribeToPot,
  unsubscribeToPot,
  checkQrCode,
  doneCapture,
}) => {
  if (!user) {
    return h(instructions, {
      checkQrCode,
      mediaSource,
      doneCapture,
    });
  }

  return brews.map(brew =>
    h(
      Brew.component,
      {
        key: [brew.id, brew.pot.id].join('.'),
        brew,
        user: user,
        now: now,
        addBrew: addBrew,
        notifications: notifications,
        subscribeToNotifications: subscribeToNotifications,
        subscribeToPot: subscribeToPot,
        unsubscribeToPot: unsubscribeToPot,
      },
    )
  );
}



const view = ({ state, actions }) => {
  const brews = state.pots.reduce((brews, pot) => {
    const matchedBrews = state.brews.filter(b => b.potId === pot.id);
    const subscription = state.subscriptions.find(sub => sub.potId === pot.id);

    if (matchedBrews.length === 0) {
      return brews.concat({
        pot,
        brewedBy: null,
        brewedAt: null,
        id: null,
        potId: pot.id,
        subscription,
      });
    }

    return brews.concat(matchedBrews.map((brew) => {
      return {
        pot,
        subscription,
        ...brew,
      };
    }));
  }, []);

  return h(
    'div',
    {
      key: 'home',
      oncreate: () => {
        actions.create(state.user);
      },
      ondestroy: () => {
        actions.destroy();
      },
      class: 'home__container',
    },
    [
      h(
        'div',
        {
          class: 'home__list',
        },
        [
          h(content, {
            brews,
            now: state.now,
            user: state.user,
            mediaSource: state.mediaSource,
            notifications: state.notifications,
            addBrew: actions.addBrew,
            subscribeToNotifications: actions.subscribeToNotifications,
            subscribeToPot: actions.subscribeToPot,
            unsubscribeToPot: actions.unsubscribeToPot,
            checkQrCode: actions.checkQrCode,
            doneCapture: actions.doneCapture,
          }),
          h(
            'div',
            {
              style: {
                paddingBottom: 'calc(92px + 2rem)',
                width: '300px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'flex-end',
                height: '575px',
              },
            },
            [
              scanQrCode(actions.checkQrCode),
            ]
          ),
        ],
      ),
      qrCodeOverlay({
        mediaSource: state.mediaSource,
        doneCapture: actions.doneCapture,
      }),
    ],
  );
};

module.exports = {
  state: initialState,
  actions: initialActions,
  view,
};
