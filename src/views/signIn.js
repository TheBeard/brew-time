const { h } = require('hyperapp');
const SignInWith = require('../components/signInWith');
const Firebase = require('../lib/firebase');

const initialState = {
};

const redirectToSearchParamThen = () => {
  const redirectTo = (window.location.search || '')
    .replace(/^\?/, '')
    .split('&')
    .find(keyValue => {
      return keyValue.startsWith('then=');
    });
  if (redirectTo) {
    path = redirectTo.split('=')[1];
    history.pushState(null, '', path);
  }
}

const initialActions = {
  signInAnonymously: (user) => (state) => {
    if (user) return;

    if (confirm('Signing in anonymously means you may lose your data if you clear cache/cookies, or use a different browser. Are you sure?')) {
      Firebase.app.auth().signInAnonymously().then(redirectToSearchParamThen);
    }
  },

  linkAccount: ({ user, provider }) => (state) => {
    if (!user) {
      return Firebase.app.auth()
        .signInWithPopup(provider)
        .then(redirectToSearchParamThen);
    }
    return user.linkWithPopup(provider);
  },

  unlinkAccount: ({ user, providerId }) => {
    console.log('unlink', user, providerId);
    user.unlink(providerId);
  },

  signOut: () => {
    console.log('signOut');
    return Firebase.module.auth().signOut();
  },

};

const view = ({ state, actions }) => h(
  'div',
  null,
  [
    h(SignInWith.component, {
      user: state.user,
      signInAnonymously: actions.signInAnonymously,
      linkAccount: actions.linkAccount,
      unlinkAccount: actions.unlinkAccount,
      signOut: actions.signOut,
    }),
  ]
);

module.exports = {
  state: initialState,
  actions: initialActions,
  view,
};
