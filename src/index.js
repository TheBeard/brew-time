const { app, h } = require('hyperapp');
const { Switch, Route, location, Link } = require('@hyperapp/router');

require('./polyfills');

const { User } = require('./lib/User');

const ServiceWorker = require('./lib/ServiceWorker');

const Nav = require('./components/Nav');

const Home = require('./views/home');
const Pot = require('./views/pot');
const SignIn = require('./views/signIn');
require('./index.styl');

const Firebase = require('./lib/firebase');

try {
  ServiceWorker.register(navigator, Firebase.app.messaging())
    .catch((err) => {
      console.log('Service worker not available', err);
    });
} catch(err) {
  console.log(err);
}

if (!(/localhost/.test(window.location.origin)) && window.location.origin !== 'https://beta.brewtime.coffee') {
  const path = window.location.pathname.replace(/^\//, '');
  window.location.replace(`https://beta.brewtime.coffee/${path}`);
}

const initialState = {
  user: new User(null),

  notifications: [],
  subscriptions: [],

  home: Home.state,
  pot: Pot.state,
  signIn: SignIn.state,

  now: Firebase.timeNow().toMillis(),

  location: location.state,
};

const initialActions = {
  setUser: firebaseUser => {
    return {
      user: User.fromUser(firebaseUser),
    };
  },

  updateNow: () => (state, actions) => {
    const now = Firebase.timeNow().toMillis();
    return {
      now,
    };
  },

  subscribeToNotifications: ({ potId, enabled }) => (state, actions) => {
    const toAdd = enabled ? [{ potId }] : [];
    const notifications = state.notifications.filter(n => n.potId !== potId).concat(toAdd);

    return Firebase.app.messaging()
      .requestPermission()
      .then(function() {
        return actions.setNotifications(notifications);
      })
      .catch(function(err) {
        console.log('Unable to get permission to notify.', err);
      });

    return {
      notifications,
    };
  },

  setNotifications: (notifications) => ({ notifications }),

  location: location.actions,

  home: Home.actions,
  pot: Pot.actions,
  signIn: SignIn.actions,
};

const renderRoutesOrLoading = (state, routes) => {
  if (state.user.isSignedIn() === undefined) {
    return [
      h(
        'h1',
        {
          key: 'loading',
        },
        ['Loading...'],
      ),
    ];
  }

  return h(
    Switch,
    null,
    routes
  );
};

const propsForView = (state, actions, route, viewName) => ({
  state: {
    now: state.now,
    user: state.user.firebaseUser,
    notifications: state.notifications,
    ...state[viewName],
  },
  actions: {
    subscribeToNotifications: actions.subscribeToNotifications,
    ...actions[viewName],
  },
  route,
});

const view = (state, actions) => h(
  'div',
  null,
  [
    h(
      Nav.component,
      {
        ...state,
        leftItems: [
        ]
      }
    ),
    h(
      'div',
      {
        style: {
          padding: '0 1rem',
        }
      },
      renderRoutesOrLoading(state, [
        h(Route, { path: '/', render: (route) => Home.view(propsForView(state, actions, route, 'home')) }),
        h(Route, { path: '/user', render: (route) => SignIn.view(propsForView(state, actions, route, 'signIn')) }),
        h(Route, { path: '/:id', render: (route) => Pot.view(propsForView(state, actions, route, 'pot')) }),
      ]),
    ),
  ]
);

const interop = app(initialState, initialActions, view, document.getElementById('app'));
location.subscribe(interop.location);
location.subscribe({
  set: (data) => {
    window.ga('send', 'pageview');
  },
  get: () => {}
});
Firebase.app.firestore().settings({ timestampsInSnapshots: true });
Firebase.app.firestore().enablePersistence().catch(console.log);
Firebase.app.auth().onAuthStateChanged(interop.setUser);
setInterval(interop.updateNow, 1000);
