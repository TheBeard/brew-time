const { h } = require('hyperapp');
const { Link } = require('@hyperapp/router');
const vagueTime = require('vague-time');

const Button = require('./Button');
const Checkbox = require('./Checkbox');
const { firebase, firebaseApp } = require('../lib/firebase');
const url = require('../lib/url');

require('./Brew.styl');

const percentage = (min, max, value) => {
  const total = max - min;
  const normalized = value - min;

  return Math.round(Math.max(0.0, Math.min(1.0, normalized / total)) * 100);
};

const Header = ({ subscription, pot, user, unsubscribeToPot }) => {
  if (!pot) return null;

  return h(
    'div',
    {
      class: 'brew__header',
    },
    [
      h('span', { class: 'brew__header-location' }, pot.location),
      h('div', { class: 'brew__header-title-container' }, [
        h(Link, { to: `/${pot.id}`, class: 'brew__header-title' }, pot.name),
      ]),

      subscription && h(
        'button',
        {
          type: 'button',
          class: 'brew__header-unsubscribe',
          onclick: () => {
            if (confirm(`Are you sure you want to unsubscribe from this ${pot.location} ${pot.name}?`)) {
              unsubscribeToPot({
                subscription,
              });
            }
          },
        },
        '×',
      ),
    ],
  );
};

const Body = ({ brew, now, subscription }) => {
  if (!brew) return null;

  const brewDoneAt = brew.brewedAt
    ? brew.brewedAt.toMillis() + brew.pot.estimatedBrewTime
    : null;

  const timeVerb = brewDoneAt
    ? (now > brewDoneAt
      ? 'Brewed'
      : 'Done'
    )
    : null;

  const percent = brew.brewedAt ? percentage(
    brew.brewedAt.toMillis(),
    brewDoneAt,
    now,
  ) : 0;

  const timeInformation = brewDoneAt
    ? vagueTime.get({ from: now, to: brewDoneAt })
    : null;

  const status = [timeVerb, timeInformation].filter(Boolean).join(' ');

  return h(
    'div',
    {
      class: 'brew__body',
    },
    [
      h('div', { class: 'brew__body-status' }, status || 'No brews'),
      h(
        'div',
        { class: 'brew__body-percentage-mug' },
        h(
          'div',
          { class: 'brew__body-percentage-inner-container' },
          h(
            'div',
            {
              class: 'brew__body-percentage-indicator',
              style: { top: `${100 - (percent / 100 * 95)}%` },
            },
          ),

          h(
            'div',
            {
              class: 'brew__body-percentage-text',
            },
            `${percent}%`,
          ),

        ),
      ),
    ],
  );
};

const FooterSubscribe = ({
  subscription,
  user,
  brew,
  subscribeToPot,
}) => {
  if (user) {
    return !brew.subscription && h(
      Button.component,
      {
        type: 'button',
        onclick: () => subscribeToPot({ potId: brew.potId, user: user }),
      },
      user ? 'Subscribe' : 'Sign in to subscribe',
    );
  }

  return h(
    'div',
    {
      style: {
        color: '#fff',
        textAlign: 'left',
        padding: '1.5rem 1rem 1rem',
        backgroundColor: 'hsl(0, 0%, 10%)',
        marginBottom: '0.5rem',
      },
    },
    [
      h('div',
        {
          style: {
            fontSize: '1.2rem',
          }
        },
        'Sign up for:'
      ),
      h('div', { style: { color: 'hsl(0, 0%, 80%)', marginBottom: '1rem' } }, [ // TODO: More style tweaks
        h('div', null, 'Notifications'),
        h('div', null, 'Brew status'),
      ]),
      h(
        Button.component,
        {
          type: 'button',
          onclick: () => history.pushState(null, '', `/user?then=${window.location.pathname}`),
          style: {
            margin: '0',
          },
        },
        'Sign up',
      ),
    ]
  );
};

const Footer = ({
  brew,
  user,
  addBrew,
  receivesNotifications,
  subscribeToNotifications,
  subscribeToPot,
}) => h(
  'div',
  {
    class: 'brew__footer',
  },
  [
    h(
      Button.component,
      {
        type: 'button',
        onclick: () => addBrew({ potId: brew.potId, user: user }),
      },
      'Start a new brew',
    ),

    h(FooterSubscribe, { user, brew, subscribeToPot }),

    user && h(
      'label',
      {
        class: 'brew__footer-notify',
      },
      [
        h(
          Checkbox.component,
          {
            id: `notifications-${brew.potId}`,
            onchange: (e) => {
              subscribeToNotifications({
                potId: brew.potId,
                enabled: e.target.checked,
              })
            },
            checked: receivesNotifications,
          },
        ),
        h('span', { class: 'brew__footer-notify-text' }, 'Notify me when it\'s ready'),
      ],
    ),
  ],
);

const debug = (...desc) => (value) => {
  console.log(...desc, value);
  return value;
};

const component = props => h(
  'div',
  {
    class: 'brew',
  },
  [
    h(
      Header,
      {
        pot: props.brew.pot,
        user: props.user,
        subscription: props.brew.subscription,
        unsubscribeToPot: props.unsubscribeToPot,
      },
    ),
    h(
      Body,
      {
        brew: props.brew,
        now: props.now,
        brewNewPot: props.brewNewPot,
      },
    ),
    h(
      Footer,
      {
        user: props.user,
        brew: props.brew,
        addBrew: props.addBrew,
        receivesNotifications: props.notifications.find(n => n.potId === props.brew.potId),
        subscribeToNotifications: props.subscribeToNotifications,
        subscribeToPot: props.subscribeToPot,
      },
    ),
    props.user && props.qrcode && h(
      'img',
      {
        src: props.qrcode,
        alt: `QR Code for ${url.forPot(props.brew.potId)}`,
        style: {
          margin: '1rem 0',
          border: '1px black solid',
          backgroundColor: 'white',
          padding: '.5rem',
        },
      }
    ),
  ],
);

module.exports = {
  component,
};
