const { h } = require('hyperapp');
const { Link } = require('@hyperapp/router');
require('./Nav.styl');

const component = ({ user, leftItems = [], location }) => h(
  'nav',
  {
    class: 'nav',
  },
  [
    h('div', { class: 'nav__inner' }, [
      h('div', { class: 'nav-group' }, [
        h(
          Link,
          { to: '/', class: 'nav-item' },
          [
            'BrewTime',
            h('span', { class: 'hosting-env' }, process.env.HOSTING_ENV),
          ],
        ),

        ...leftItems.map(([text, path]) => (
          Link({ to: path, className: 'nav-item' }, text)
        )),
      ]),
      h('div', { class: 'nav-group' }, [
        h(
          'button',
          {
            type: 'button',
            class: 'nav-item',
            onclick: (e) => {
              e.preventDefault();
              history.pushState(null, '', '/user');
            },
          },
          user.isSignedIn() ? user.displayName() : 'Sign in'
        ),
      ]),
    ]),
  ]
);

module.exports = {
  component,
};
