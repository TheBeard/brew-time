const { h } = require('hyperapp');
require('./Button.styl');

const component = (props, children) => h(
  'button',
  {
    ...props,
    class: ['button'].concat(props.class).filter(Boolean).join(' ')
  },
  children,
);

module.exports = {
  component,
};
