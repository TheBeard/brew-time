const { h } = require('hyperapp');
require('./signInWith.styl');

const Firebase = require('../lib/firebase');

const panel = (props, children) => h(
  'div',
  {
    class: 'sign-in-with__panel',
  },
  [
    props.title,
    ...children,
  ],
);

const signInWith = ({ isAnonymous, user, linkAccount, signInAnonymously }) => [
  isAnonymous
    && h('p', null, 'You are signed in anonymously, and you may lose your data between sessions or browsers. Link an extra account here to keep your data.'),
  !user && h(
    'button',
    {
      type: 'button',
      class: 'sign-in-with-provider firebase',
      onclick: (e) => {
        e.preventDefault();
        signInAnonymously(user);
      },
    },
    'Anonymous Account',
  ),
  h(
    'button',
    {
      type: 'button',
      class: 'sign-in-with-provider github',
      onclick: (e) => {
        e.preventDefault();
        linkAccount({
          user,
          provider: new Firebase.module.auth.GithubAuthProvider(),
        })
      },
    },
    'Github',
  ),
  h(
    'button',
    {
      type: 'button',
      class: 'sign-in-with-provider google',
      onclick: () => {
        linkAccount({
          user,
          provider: new Firebase.module.auth.GoogleAuthProvider(),
        })
      },
    },
    'Google',
  ),
];

const component = ({ user, signInAnonymously, linkAccount, unlinkAccount, signOut }) => {
  const identities = [user]
    .concat(user ? user.providerData : [])
    .filter(Boolean)
    .filter(provider => provider.providerId !== 'firebase');

  const isAnonymous = user
    && user.isAnonymous
    && identities.length === 0;

  return h(
    'div',
    {
      class: 'sign-in-with',
    },
    [
      identities.length > 0 && h(
        panel,
        { title: 'Attached identities' },
        [
          ...identities.map((userInfo) =>
            h(
              'button',
              {
                type: 'button',
                disabled: identities.length <= 1,
                class: ['sign-in-with-provider', userInfo.providerId].join(' '),
                onclick: (e) => {
                  e.preventDefault();
                  unlinkAccount({ user, providerId: userInfo.providerId });
                },
              },
              `Unlink ${userInfo.providerId}`,
            )
          ),
        ],
      ),
      h(panel, { title: 'Sign in with...' }, signInWith({ user, linkAccount, signInAnonymously, isAnonymous })),
      user && h(panel, { title: 'Session' }, [
        h(
          'button',
          {
            type: 'button',
            class: 'sign-in-with-provider',
            onclick: signOut,
          },
          'Sign Out',
        ),
      ]),
    ]
  );
}

module.exports = {
  component,
};
