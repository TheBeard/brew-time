const { h } = require('hyperapp');
require('./Checkbox.styl');

const component = (props, children) => h(
  'span',
  null,
  [
    h(
      'input',
      {
        ...props,
        type: 'checkbox',
        class: ['check-box'].concat(props.class).filter(Boolean).join(' '),
      }
    ),
    h(
      'label',
      {
        for: props.id,
        class: 'check-box',
      },
      children,
    ),
  ],
);

module.exports = {
  component,
};
