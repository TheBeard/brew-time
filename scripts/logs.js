require('dotenv').config();
const admin = require('firebase-admin');
const serviceAccount = require('../data/serviceAccount.json');

const credential = admin.credential.cert(serviceAccount);

const app = admin.initializeApp({
  credential,
  databaseURL: 'https://brew-time-edc30.firebaseio.com',
});

const firestore = app.firestore();

firestore.settings({
  timestampsInSnapshots: true,
});

firestore
  .collection('logs')
  .orderBy('createdAt', 'asc')
  .limit(200)
  .onSnapshot((snap) => {
    snap.docs.forEach((logSnap) => {
      const log = logSnap.data();
      console.log(`${(new Date(log.createdAt.toMillis())).toISOString()} - ${log.event} - ${log.message}`);
    });
  });
