const path = require('path');
const webpack = require('webpack');
const { execSync } = require('child_process');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const convert = require('koa-connect');
const history = require('connect-history-api-fallback');
require('dotenv').config();

const gitCommitSha = execSync('git rev-parse HEAD').toString().trim()
const hostingEnvironment = process.env.HOSTING_ENV || 'beta';

module.exports = {
  mode: process.env.WEBPACK_SERVE ? 'development' : 'production',
  module: {
    rules: [
      {
        test: /\.styl$/,
        use: ['style-loader', 'css-loader', 'stylus-loader'],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.FIREBASE_API_KEY': JSON.stringify(process.env.FIREBASE_API_KEY),
      'process.env.FIREBASE_AUTH_DOMAIN': JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
      'process.env.FIREBASE_PROJECT_ID': JSON.stringify(process.env.FIREBASE_PROJECT_ID),
      'process.env.FIREBASE_MESSAGING_SENDER_ID': JSON.stringify(process.env.FIREBASE_MESSAGING_SENDER_ID),
      'process.env.PUBLIC_VAPID_KEY': JSON.stringify(process.env.PUBLIC_VAPID_KEY),
      'process.env.HOSTING_ENV': JSON.stringify(hostingEnvironment),
    }),
    new CopyWebpackPlugin([
      {
        from: './src/statics/service-worker.js',
        to: './service-worker.js',
        transform: (content, filePath) => {
          const newContent = content
            .toString('utf8')
            .replace(/__SERVICE_WORKER_VERSION__/, gitCommitSha);

          return Buffer.from(newContent);
        },
      },
      { from: './src/statics/manifest.json', to: './manifest.json' },
      { from: './src/statics/icon.svg', to: './icon.svg' },
      { from: './src/statics/icon-144.png', to: './icon-144.png' },
      { from: './src/statics/index.html', to: './index.html' },
    ]),
    new webpack.BannerPlugin({
      banner: `\nVersion ${gitCommitSha}, built ${(new Date()).toISOString()}\n`,
      entryOnly: true,
    }),
  ],
  serve: {
    host: '0.0.0.0',
    add: (app, middleware, options) => {
      app.use(convert(history()));
    },
  },
};
